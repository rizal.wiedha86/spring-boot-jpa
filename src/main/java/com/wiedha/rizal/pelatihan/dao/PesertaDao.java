package com.wiedha.rizal.pelatihan.dao;

import com.wiedha.rizal.pelatihan.entity.Peserta;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PesertaDao extends PagingAndSortingRepository<Peserta, String> {


}
