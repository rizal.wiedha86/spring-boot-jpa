package com.wiedha.rizal.pelatihan;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AplikasiPelatihanApplication.class)
public class AplikasiPelatihanApplicationTests {

	@Test
	void contextLoads() {
	}

}
