package com.wiedha.rizal.pelatihan.dao;

import com.wiedha.rizal.pelatihan.AplikasiPelatihanApplication;
import com.wiedha.rizal.pelatihan.entity.Peserta;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Optional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AplikasiPelatihanApplication.class)
@Sql(
        executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD,
        scripts = "/data/peserta.sql"
)

public class PesertaDaoTest {

    @Autowired
    private PesertaDao pd;

    @Autowired
    private DataSource ds;




    @AfterEach
    public void hapusData() throws Exception {
        String sql = "delete from peserta where email = 'peserta001@gmail.com'";
        try (Connection c = ds.getConnection()) {
            c.createStatement().executeUpdate(sql);

        }
    }

    @Test
    public void testInsert() throws SQLException {
        Peserta p = new Peserta();
        p.setNama("Peserta 001");
        p.setEmail("peserta001@gmail.com");
        p.setTanggalLahir(new Date());

        pd.save(p);

        String sql = "SELECT count(*) AS jumlah FROM peserta WHERE email = 'peserta001@gmail.com'";

        try (Connection c = ds.getConnection()) {

            ResultSet rs = c.createStatement().executeQuery(sql);
            Assert.assertTrue(rs.next());

            Long jumlahRow = rs.getLong("jumlah");
            Assert.assertEquals(1L, jumlahRow.longValue());

        }
    }

    @Test
    public void testHitung() {
        Long jumlah = pd.count();
        Assert.assertEquals(3L, jumlah.longValue());
    }

    @Test
    public void testCariById(){
        Optional<Peserta> p = pd.findById("aa1");
        Assert.assertNotNull(p);
        Assert.assertEquals("Peserta Test 001", p.get().getNama());
        Assert.assertEquals("peserta.test.001@gmail.com", p.get().getEmail());

        //Optional<Peserta> px = pd.findById("xx");
        //Assert.assertNull(px);
    }



}
